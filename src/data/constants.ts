// Global variables
export const seoTitle = "Le Chant des Pistes | Compagnie de Théâtre";

export const seoDescription =
  "Compagnie de théâtre basée à Grenoble. Création de spectacles en salles et territoires de montagne. Projets en milieu naturel.";

const sizeMax = 140;
if (seoDescription.length > sizeMax) {
  throw new Error(
    `La taille maximale de la description pour le SEO est de ${sizeMax} caractères.`,
  );
}
