---
title: → Contacts
description: ""
displayInNavbar: 2
---

```
    → DIRECTION
```

**Maïssa Boukehil** (administration)  
 +33 6 74 38 58 97  
 _administration [AT] lechantdespistes.fr_

**Jérôme Cochet** (artistique)  
 +33 6 26 74 90 44  
 _cochet.j [AT] gmail.com_

**Caroline Mas** (technique)  
 +33 6 31 51 97 18  
 _technique [AT] lechantdespistes.fr_

      → BUREAU

**Marion Douarche** (présidente)

**Floriane Duthel** (trésorière)

      → LE CHANT DES PISTES

21 rue Boucher de Perthes — 38000 GRENOBLE

_association loi 1901_

**Licences**  2023-008058/008059  
**SIRET** 924 246 002 00019  
**APE** 9001 Z

</center>
<!--**Facebook** [Les Non Alignés](https://www.facebook.com/Les-Non-Align%C3%A9s-191755984505554/)-->
