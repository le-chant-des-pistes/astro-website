---
title: Fabrication du site
description: Éléments de contexte sur la fabrication de ce site web
---

| Site web à façon       |                                                                                                                              |
| ---------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| Artisan                | <a href="https://fr.linkedin.com/in/sdupouy" target="_blank">Séverin Dupouy</a>                                              |
| Première mise en ligne | **mars 2024**                                                                                                                |
| Outillage              | <a href="https://astro.build/" target="_blank">astro</a> + <a href="https://tailwindcss.com" target="_blank">tailwindcss</a> |
