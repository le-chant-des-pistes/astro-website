---
title: → Calendrier
description: ""
displayInNavbar: 2
---

<iframe width="100%" height="400" frameborder="0" allowfullscreen allow="geolocation" src="//framacarte.org/fr/map/tournee-mdm-24-25_178131?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&editMode=disabled&moreControl=false&searchControl=null&tilelayersControl=false&embedControl=false&datalayersControl=false&onLoadPanel=none&captionBar=false&captionMenus=true&captionControl=true&locateControl=false&measureControl=false&editinosmControl=false&starControl=null#6/46.400/3.362"></iframe>

## 2025

|                       |                         |                                                |                                                                                                                     |
| --------------------- | ----------------------- | ---------------------------------------------- | ------------------------------------------------------------------------------------------------------------------- |
| 16 et 17 janvier 2025 | **Mort d'Une Montagne** | Théâtre de la tête noire, Saran (45)           | [+d'infos](http://www.theatre-tete-noire.com/spectacle/mort-d-une-montagne/230)                                     |
| 21 janvier 2025       | **Mort d'Une Montagne** | Théâtre de Saint Lô (50)                       | [+d'infos](https://www.saint-lo.fr/decouvrir-bouger/culture/theatre/150-programme-theatre)                          |
| 23 janvier 2025       | **Mort d'Une Montagne** | Le Cube, Douvres-la-Délivrande (14)            | [+d'infos](https://www.c3lecube.fr/evenement/theatre/mort-d-une-montagne-/428)                                      |
| 25 janvier 2025       | **Mort d'Une Montagne** | Centre Culturel Georges Brassens, Avrillé (49) | [+d'infos](https://lesondufresnel.org/avrille-2024-2025/)                                                           |
| 22 février 2025       | **Feu la Forêt**        | La Crypte, Lagorce (07) - **lecture**          | [+d'infos](https://www.lignesdhorizon.org/parcours-eac)                                                             |
| 4 mars 2025           | **Mort d'Une Montagne** | Grain de Sel, Séné (56)                        | [+d'infos](https://www.graindesel.bzh/evenement/spectacle-vivant/mort-dune-montagne/)                               |
| 6 mars 2025           | **Mort d'Une Montagne** | Le Quai des arts, Pornichet (44)               | [+d'infos](https://quaidesarts-pornichet.fr/%C3%A9v%C3%A8nement/mort-dune-montagne/)                                |
| 8 mars 2025           | **Mort d'Une Montagne** | Théâtre Philippe Noiret, Doué-en-Anjou (49)    | [+d'infos](https://tpndoueanjou.mapado.com/event/430001-mort-dune-montagne-cie-le-chant-des-pistes)                 |
| 11 mars 2025          | **Mort d'Une Montagne** | L’Entracte, Sarblé-sur-Sarthe (72)             | [+d'infos](https://lentracte-sable.fr/mort-dune-montagne/)                                                          |
| 13 mars 2025          | **Mort d'Une Montagne** | Le Quatrain, Haute-Goulaine (44)               | [+d'infos](https://www.lequatrain.fr/)                                                                              |
| 14 mars 2025          | **Mort d'Une Montagne** | L’Equinoxe, Savenay (44)                       | [+d'infos](https://billetterie-ville-de-savenay.mapado.com/event/422634-mort-dune-montagne-cie-le-chant-des-pistes) |
| 19 mars 2025          | **Mort d'Une Montagne** | Poly'Sons, Noyarey (38)                        |                                                                                                                     |
| 22 mars 2025          | **Mort d'Une Montagne** | Bourg Saint-Maurice (73)                       | [+d'infos](https://agirpourlesglaciers.org/)                                                                        |
| 4 avril 2025          | **Mort d'Une Montagne** | Le Caméléon, Pont-du-Château (63)              | [+d'infos](http://lesrdvducameleon.pontduchateau.fr/)                                                               |
| 12 avril 2025         | **Mort d'Une Montagne** | L’Iris, Francheville (69)                      | [+d'infos](https://culture.mairie-francheville69.fr/saison-culturelle-2024-2025-a-liris/)                           |
| 18 avril 2025         | **Mort d'Une Montagne** | Le Coléo, Pontcharra (73)                      | [+d'infos](https://pontcharra.fr/570-le-coleo-la-saison-2024-2025.htm)                                              |
| 24 avril 2025         | **Mort d'Une Montagne** | Le Préo Scène, Oberhausbergen (67)             | [+d'infos](https://www.le-preo.fr/evenement/mort-dune-montagne/)                                                    |
| 22 mai 2025           | **Mort d'Une Montagne** | Théâtre Jean Carmet, Mornant (69)              | [+d'infos](https://theatre-cinema-jean-carmet.fr/spectacles/saison-2024-25.html)                                    |
| 5 juin 2025           | **Mort d'Une Montagne** | Aérodrôme de Romanin, St-Rémy-de-Provence (13) | [+d'infos](https://www.mairie-saintremydeprovence.com/events/mort-dune-montagne/)                                   |

## 2024

|                     |                         |                                              |                                                                                                                                                                                                                                                          |
| ------------------- | ----------------------- | -------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 11 janvier 2024     | **Mort d'Une Montagne** | Le Prisme, Seyssins (38)                     | [+d'infos](https://www.seyssins.fr/188-programmation-culturelle.htm)                                                                                                                                                                                     |
| 25 janvier 2024     | **Mort d'Une Montagne** | L'Amphithéâtre, Pont de Claix (38)           | [+d'infos](https://www.pontdeclaix.fr/saison-culturelle/prochaines-dates)                                                                                                                                                                                |
| 27 janvier 2024     | **Mort d'Une Montagne** | Modane (73)                                  | [+d'infos](https://www.haute-maurienne-vanoise.com/en/festival-and-event/theatre-mort-dune-montagne-modane/)                                                                                                                                             |
| 03 février 2024     | **Mort d'Une Montagne** | La Tour du Pin (38)                          | [+d'infos](https://www.latourdupin.fr/culturelle-et-sportive/saison-culturelle/programme/)                                                                                                                                                               |
| 06 février 2024     | **Mort d'Une Montagne** | Cultur\(r)al, Sallanches (74)                | [+d'infos](https://sallanchesmontblanc.com/saison-culturelle-culturral)                                                                                                                                                                                  |
| 08 février 2024     | **Mort d'Une Montagne** | ECM, Bourg-de-Thizy (69)                     | [+d'infos](https://theatredevillefranche.com/spectacles/mort-dune-montagne-ecm/)                                                                                                                                                                         |
| 09 février 2024     | **Mort d'Une Montagne** | Salle des Fêtes, Les Sauvages (69)           | [+d'infos](https://theatredevillefranche.com/spectacles/mort-dune-montagne-ecm/)                                                                                                                                                                         |
| 15 février 2024     | **Mort d'Une Montagne** | L'Aqueduc, Dardilly (69)                     | [+d'infos](http://aqueduc.dardilly.fr/programmation/)                                                                                                                                                                                                    |
| 12 mars 2024        | **Mort d'Une Montagne** | Scènes du Jura, Lons-le-Saunier (39)         | [+d'infos](https://www.scenesdujura.com/les-spectacles.htm)                                                                                                                                                                                              |
| 27 avril 2024       | **Mort d'Une Montagne** | La Tour d'Auvergne (63)                      | [+d'infos](https://www.domes-sancyartense.fr/culture-sport-vie-associative/saison-culturelle-artenscene/)                                                                                                                                                |
| 17 mai 2024         | **Feu la Forêt**        | St-Antoine-l'Abbaye (38)                     |                                                                                                                                                                                                                                                          |
| 1er juin 2024       | **Mort d'Une Montagne** | St-Pierre-de-Belleville (73)                 |                                                                                                                                                                                                                                                          |
| 22 juin 2024        | **Mort d'Une Montagne** | Vallouise-Pelvoux (05)                       | annulée                                                                                                                                                                                                                                                  |
| 26 juillet 2024     | **Feu la Forêt**        | Textes en L'Air, St-Antoine-l'Abbaye (38)    | [+d'infos](https://textesenlair.fr/)                                                                                                                                                                                                                     |
| 30 juillet 2024     | **Mort d'Une Montagne** | Nuits de la Mayenne, Changé (53)             | [+d'infos](https://www.nuitsdelamayenne.com/)                                                                                                                                                                                                            |
| 31 juillet 2024     | **Mort d'Une Montagne** | Nuits de la Mayenne, Lignères-Orgères (53)   | [+d'infos](https://www.nuitsdelamayenne.com/)                                                                                                                                                                                                            |
| 4 août 2024         | **Mort d'Une Montagne** | Bistrot Lou Fresc, Méolan-Revel (04)         | [+d'infos](https://loufresc.com/rendez-vous/)                                                                                                                                                                                                            |
| 3 août 2024         | **Mort d'Une Montagne** | Lou Fresc, Méolans-Revel (04)                | [+d'infos](http://loufresc.com/rendez-vous/)                                                                                                                                                                                                             |
| 6 août 2024         | **Mort d'Une Montagne** | Refuge de Chambeyron, St-Paul-sur-Ubaye (04) | [+d'infos](https://refugeduchambeyron.ffcam.fr/FR_home.html)                                                                                                                                                                                             |
| 8 août 2024         | **Mort d'Une Montagne** | Les Claps de l'Ubaye, St-Paul-sur-Ubaye (04) | [+d'infos](https://les-claps-de-lubaye.jimdosite.com/)                                                                                                                                                                                                   |
| 9 août 2024         | **Mort d'Une Montagne** | Vallouise-Pelvoux (05)                       | [+d'infos](https://www.paysdesecrins.com/)                                                                                                                                                                                                               |
| 9 septembre 2024    | **Mort d'Une Montagne** | ENSA, Chamonix (74)                          | [+d'infos](https://www.ensa.sports.gouv.fr/)                                                                                                                                                                                                             |
| 12 septembre 2024   | **Mort d'Une Montagne** | Musée des Merveilles, Tende (06)             | [+d'infos](https://museedesmerveilles.departement06.fr/)                                                                                                                                                                                                 |
| 13 septembre 2024   | **Mort d'Une Montagne** | Refuge des Merveilles, Tende (06)            | [+d'infos](https://refugedesmerveilles.ffcam.fr/FR_home.html)                                                                                                                                                                                            |
| 14 septembre 2024   | **Mort d'Une Montagne** | Refuge de Nice (06)                          | [+d'infos](https://refugedenice.ffcam.fr/FR_home.html)                                                                                                                                                                                                   |
| 3 octobre 2024      | **Mort d'Une Montagne** | Salon du livre, Bagnères-de-Bigorre (65)     | [+d'infos](https://www.salondulivre-pyreneen.fr/)                                                                                                                                                                                                        |
| 8 et 9 octobre 2024 | **Mort d'Une Montagne** | L’Escale, Tournefeuille (31) avec Odyssud    | [+d'infos](hhttps://lescale-tournefeuille.fr/les_spectacles/mort-dune-montage/)                                                                                                                                                                          |
| 12 octobre 2024     | **Mort d'Une Montagne** | Orlu (09)                                    |                                                                                                                                                                                                                                                          |
| 15 octobre 2024     | **Mort d'Une Montagne** | Théâtre de Cusset (03)                       | [+d'infos](https://www.ville-cusset.com/evenement/mort-dune-montagne/)                                                                                                                                                                                   |
| 25 octobre 2024     | **Feu la Forêt**        | TMG Grenoble (38) - LECTURE                  |                                                                                                                                                                                                                                                          |
| 26 octobre 2024     | **Feu la Forêt**        | Saint Ismier (38) - LECTURE                  | [+d'infos](https://forestivites.fr/evenement/85_feu-la-foret)                                                                                                                                                                                            |
| 27 octobre 2024     | **Feu la Forêt**        | Le Haut Bréda (38) - ATELIER SON             | [+d'infos](https://forestivites.fr/evenement/87_atelier-sonore-feu-la-foret)                                                                                                                                                                             |
| 2 novembre 2024     | **Mort d'Une Montagne** | Vallon Pont d'Arc (07)                       | [+d'infos](https://www.lignesdhorizon.org/)                                                                                                                                                                                                              |
| 13 novembre 2024    | **Mort d'Une Montagne** | La Motte Servolex (73)                       | [+d'infos](https://www.mairie-lamotteservolex.fr/sortir-et-bouger/saison-culturelle/)                                                                                                                                                                    |
| 15 novembre 2024    | **Mort d'Une Montagne** | Le Péage de Roussillon (38)                  | [+d'infos](https://www.travailetculture.com/saison-culturelle/mort-montagne/)                                                                                                                                                                            |
| 16 novembre 2024    | **Feu la Forêt**        | Le Pontet (38) - LECTURE                     | [+d'infos](https://belledonne-et-veillees.com/veillee-de-la-vallee-des-huiles/)                                                                                                                                                                          |
| 21 novembre 2024    | **Mort d'Une Montagne** | Le Jardin de verre, Cholet (49)              | [+d'infos](https://www.jardindeverre.fr/evenement/mort-dune-montagne/)                                                                                                                                                                                   |
| 23 novembre 2024    | **Mort d'Une Montagne** | St-Jean-de-Boiseau (44)                      | [+d'infos](https://www.saint-jean-de-boiseau.fr/culture-sports-et-decouverte/la-saison-culturelle/)                                                                                                                                                      |
| 26 novembre 2024    | **Mort d'Une Montagne** | Juliobona, Lillebonne (76)                   | [+d'infos](https://www.juliobona.fr/mdum)                                                                                                                                                                                                                |
| 28 novembre 2024    | **Mort d'Une Montagne** | Le Rive Gauche, St-Etienne-du-Rouvray (76)   | [+d'infos](https://www.lerivegauche76.fr/evenement/mort-dune-montagne/)                                                                                                                                                                                  |
| 10 décembre 2024    | **Mort d'Une Montagne** | Grenoble, INP - ENSe3 (38)                   |                                                                                                                                                                                                                                                          |
| 14 décembre 2024    | **Mort d'Une Montagne** | Les Orres (05)                               | [+d'infos](https://www.facebook.com/events/1629499504444565/?acontext=%7B%22event_action_history%22%3A[%7B%22surface%22%3A%22search%22%7D%2C%7B%22mechanism%22%3A%22attachment%22%2C%22surface%22%3A%22newsfeed%22%7D]%2C%22ref_notif_type%22%3Anull%7D) |

## 2023

|                    |                         |                                            |
| ------------------ | ----------------------- | ------------------------------------------ |
| 12 janvier 2023    | **Mort d'Une Montagne** | Quai des Arts, Rumilly (74)                |
| 20 janvier 2023    | **Mort d'Une Montagne** | Théâtre de Châtillon (92)                  |
| 25 janvier 2023    | **Mort d'Une Montagne** | Le Cairn, Lans en Vercors (38)             |
| 27 janvier 2023    | **Mort d'Une Montagne** | Joyeuse (07)                               |
| 25 février 2023    | **Mort d'Une Montagne** | Saint-Gervais-Mont-Blanc (74)              |
| 26 février 2023    | **Mort d'Une Montagne** | Scientrier (74)                            |
| 07 au 11 mars 2023 | **Mort d'Une Montagne** | La Comédie de Valence (26)                 |
| 14 mars 2023       | **Mort d'Une Montagne** | CC Charlie Chaplin, Vaulx-en-Velin (69)    |
| 16 et 17 mars 2023 | **Mort d'Une Montagne** | Théâtre de Bourg en Bresse (01)            |
| 22 mars 2023       | **Mort d'Une Montagne** | Théâtre de Riom (63)                       |
| 30 mai 2023        | **Mort d'Une Montagne** | Saint-Julien en Genevois (74)              |
| 14 juin 2023       | **Mort d'Une Montagne** | Lus-la-Croix-Haute (38)                    |
| 15 juin 2023       | **Mort d'Une Montagne** | Valdrôme (26)                              |
| 16 juin 2023       | **Mort d'Une Montagne** | Saint-Agnan en Vercors (26)                |
| 17 juin 2023       | **Mort d'Une Montagne** | Luc-en-Diois (26)                          |
| 25 juin 2023       | **Mort d'Une Montagne** | Duingt - Annecy (74)                       |
| 29 juin 2023       | **Mort d'Une Montagne** | Jardin du Lautaret (05)                    |
| 30 juin 2023       | **Mort d'Une Montagne** | Villar d'Arène (05)                        |
| 02 juillet 2023    | **Mort d'Une Montagne** | La Gélinotte Freydières (38)               |
| 28 juillet 2023    | **Mort d'Une Montagne** | Saint Antoine l'Abbaye (38)                |
| 30 juillet 2023    | **Mort d'Une Montagne** | Saint-Hilaire-du-Rosier (38)               |
| 01 août 2023       | **Mort d'Une Montagne** | Refuge de la Leisse (73)                   |
| 03 août 2023       | **Mort d'Une Montagne** | Bramans (73)                               |
| 05 août 2023       | **Mort d'Une Montagne** | Refuge des Aiguilles d'Arves (73)          |
| 13 août 2023       | **Horizon(S)**          | Malay (71)                                 |
| 14 août 2023       | **Horizon(S)**          | Camping Les Mésanges, Lac des Settons (71) |
| 16 août 2023       | **Horizon(S)**          | Le Relais des Passages, Pellevoisin (36)   |
| 17 août 2023       | **Horizon(S)**          | La Secousse, Jeu-Les-Bois (36)             |
| 22 août 2023       | **Horizon(S)**          | Le Potager Culturel, Beaumont (63)         |
| 13 septembre 2023  | **Mort d'Une Montagne** | L'Avant Scène, Laval (53)                  |
| 16 septembre 2023  | **Mort d'Une Montagne** | Refuge des Merveilles (06)                 |
| 17 septembre 2023  | **Mort d'Une Montagne** | La Brigue (06)                             |
| 28/29 sept. 2023   | **Mort d'Une Montagne** | L'Usine à Gaz - Nyon (Suisse)              |
| 30 septembre 2023  | **Mort d'Une Montagne** | Barraux (38)                               |
| 03 octobre 2023    | **Mort d'Une Montagne** | L'Estive, Foix (09)                        |
| 06 octobre 2023    | **Mort d'Une Montagne** | EMC2, Chamonix (74)                        |
| 19 octobre 2023    | **Mort d'Une Montagne** | La Ricamarie (42)                          |
| 21 octobre 2023    | **Mort d'Une Montagne** | Saint-Nazaire-les-Eymes (38)               |
| 16 novembre 2023   | **Mort d'Une Montagne** | L'Odéon, Nîmes (30)                        |
| 17 novembre 2023   | **Mort d'Une Montagne** | Clarensac (30)                             |
| 24 novembre 2023   | **Mort d'Une Montagne** | Chaussy (95)                               |
| 05 décembre 2023   | **Mort d'Une Montagne** | Le Beffroi, Montrouge (92)                 |
| 07 décembre 2023   | **Mort d'Une Montagne** | Le Bordeau, Saint Genis Pouilly (01)       |
| 13-15 déc. 2023    | **Mort d'Une Montagne** | TNP, Villeurbanne (69)                     |
| 19 décembre 2023   | **Mort d'Une Montagne** | Samoëns (74)                               |

## 2022

|                     |                         |                                                                                         |
| ------------------- | ----------------------- | --------------------------------------------------------------------------------------- |
| 6 janvier 2022      | **Mort d'Une Montagne** | [présentation à la Route des 20](https://www.youtube.com/embed/bOjmqaeauxo?start=11911) |
| 18-22 jan. 2022     | **Mort d'Une Montagne** | représentations à Lyon, Point du Jour (69)                                              |
| 26/27 jan. 2022     | **Mort d'Une Montagne** | représentations à Grenoble, TMG (38)                                                    |
| 4, 5, 6 fev. 2022   | **Destin(S)**           | représentations à Lyon, Théâtre de l'Elysée (38)                                        |
| 17 mai 2022         | **Destin(S)**           | représentations à Die, Théâtre les Aires (26)                                           |
| 26 mai 2022         | **Mort d'Une Montagne** | La Pierre (38) - Les Rendez-Vous au Manoir                                              |
| 28 mai 2022         | **Mort d'Une Montagne** | La Pierre (38) - Les Rendez-Vous au Manoir                                              |
| 28 mai 2022         | **Destin(S)**           | La Pierre (38) - Les Rendez-Vous au Manoir                                              |
| 3 et 4 juin 2022    | **Terres d'en-Haut #2** | Saint-Julien en Genevois (74)                                                           |
| 01 juillet 2022     | **Mort d'Une Montagne** | Villeurbanne, TNP (69) - Prix Incandescences                                            |
| 03 août 2022        | **Mort d'Une Montagne** | (extraits) à Genève - Théâtre du Loup                                                   |
| 05 août 2022        | **Mort d'Une Montagne** | Les Villards Sur Thônes (74) - La Fourmillante                                          |
| 06 août 2022        | **Mort d'Une Montagne** | Refuge de Gramusset (74)                                                                |
| 07 août 2022        | **Mort d'Une Montagne** | Refuge du Parmelan (74)                                                                 |
| 13 août 2022        | **Horizon(S)**          | Le Mas du Tilleul, Eygalières (13)                                                      |
| 15 août 2022        | **Horizon(S)**          | Rossas, Valdrôme (26)                                                                   |
| 16 août 2022        | **Horizon(S)**          | Glandage (26)                                                                           |
| 17 août 2022        | **Horizon(S)**          | La Jarjatte, Lus-la-Croix-Haute (26)                                                    |
| 27 septembre 2022   | **Mort d'Une Montagne** | Cinéma de Douvaine (74)                                                                 |
| 28 septembre 2022   | **Mort d'Une Montagne** | Palais des Sports de Morzine (74)                                                       |
| 29 septembre 2022   | **Mort d'Une Montagne** | Théâtre du Casino d'Evian (74)                                                          |
| 01 octobre 2022     | **Mort d'Une Montagne** | Salle des Fêtes de St Colomban des Villards (73)                                        |
| 03 octobre 2022     | **Mort d'Une Montagne** | La Rotonde, INSA Lyon (69)                                                              |
| 05 octobre 2022     | **Mort d'Une Montagne** | Moûtiers (73)                                                                           |
| 06 octobre 2022     | **Mort d'Une Montagne** | La Chaudanne, Arêches-Beaufort (73)                                                     |
| 07 octobre 2022     | **Mort d'Une Montagne** | Salle des Fêtes de Pallud (73)                                                          |
| 08 octobre 2022     | **Mort d'Une Montagne** | Salle Chorus, Bourg-Saint-Maurice (73)                                                  |
| 09 novembre 2022    | **Spéléo Secours Live** | Ciné Montagne, Grenoble (38)                                                            |
| 18 novembre 2022    | **Mort d'Une Montagne** | Congrès National SNAM, Montdauphin (05)                                                 |
| 08/09 décembre 2022 | **Mort d'Une Montagne** | TMG Grenoble, Théâtre 145 (38)                                                          |
| 17 décembre 2022    | **Horizon(S)**          | Fête du Solstice, Montoison (26)                                                        |

## 2021

|                  |                         |                                                              |
| ---------------- | ----------------------- | ------------------------------------------------------------ |
| 14 octobre 2021  | **Mort d'Une Montagne** | représentation aux Adrets (38)                               |
| 31 juillet 2021  | **Mort d'Une Montagne** | représentation à la Bérarde (38)                             |
| 28 juillet 2021  | **Mort d'Une Montagne** | représentation au Chazelet - La Grave (38)                   |
| 27 juillet 2021  | **Mort d'Une Montagne** | représentation à Pontcharra (38)                             |
| 4 juin 2021      | **Horizon(S)**          | représentation, Fest. de la Basse-Cour                       |
| 5 et 6 juin 2021 | **Horizon(S)**          | deux représentations au théâtre de l'Elysée, Lyon            |
| 20 juin 2021     | **Horizon(S)**          | deux représentations à Mainvilliers                          |
| 11 mai 2021      | **Horizon(S)**          | deux représentations au collège Clémenceau, Lyon (scolaires) |
| 10 mai 2021      | **Horizon(S)**          | représentation à l'INSA de Lyon (scolaire)                   |

## 2020

|                    |                         |                                            |
| ------------------ | ----------------------- | ------------------------------------------ |
| 20 septembre 2020  | **Horizon(S)**          | deux représentations à Die (26)            |
| 19 septembre 2020  | **Mort d'Une Montagne** | restitution à la Pierre (38)               |
| 18 septembre 2020  | **Mort d'Une Montagne** | extraits à Saint-Fons (69)                 |
| 21 août 2020       | **Horizon(S)**          | représentation à Laval (38)                |
| 19 août 2020       | **Horizon(S)**          | représentation à Revel (Freydières) (38)   |
| 17 août 2020       | **Horizon(S)**          | représentation à St Hilaire du Rosier (38) |
| 15 et 16 août 2020 | **Horizon(S)**          | représentations à Sainte Croix (26)        |
| 25 juillet 2020    | **Mort d'Une Montagne** | présentation à Freydières (38)             |
| 24 juillet 2020    | **Mort d'Une Montagne** | présentation aux Adrets (38)               |
| 19 juillet 2020    | **Mort d'Une Montagne** | présentation à la Berarde (38)             |
| 16 juillet 2020    | **Horizon(S)**          | représentation à Vaulx-en-Velin (69)       |
| 5 juillet 2020     | **Mort d'Une Montagne** | marche-lecture en Belledonne (38)          |
| 10 juin 2020       | **Mort d'Une Montagne** | restitution à Laval (Belledonne, 38)       |
