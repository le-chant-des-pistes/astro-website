---
title: La Compagnie
description: ""
displayInNavbar: 1
---

---

> Il n'y avait pas un rocher, pas une rivière dans le pays qui ne pouvait être ou n'avait pas été chantée.
>
> #### _Le Chant des Pistes_, Bruce Chatwin

---

La compagnie _Le Chant des Pistes_ est l'une des deux branches issue des _Non Alignés_, collectif artistique fondé en 2015 à Lyon. Elle en poursuit les projets de création, de territoire et de transmission à partir de la saison 2023-2024, et affirme son ancrage construit en Auvergne-Rhône-Alpes au cours des dernières années par le déplacement de son siège social de Lyon vers Grenoble.

Notre vocation, avec _Le Chant des Pistes_, est de produire un théâtre de terrain proche du réel, accueillant et accessible, à même de se déployer à la fois sous des formes légères et nomades, comme dans des versions scéniques ambitieuses pouvant se diffuser dans les plus grandes salles.

Le développement de cet outil de création est aussi l'occasion d'amorcer un nouveau cycle dans nos projets, ouvrant nos pratiques à d'autres collaborations et territoires, portés par le goût de l'aventure, du croisement des regards, dans une démarche de durabilité face aux transformations profondes du monde en cours et à venir.

La compagnie voyage entre les lieux et espaces qui l'accueillent en résidence, création et diffusion, au gré des rencontres humaines et de ses projets, depuis les cimes enneigées jusqu'aux océans, passant par les forêts profondes comme par les plateaux des théâtres.

    ↓ LE CHANT DES PISTES

        → Jérôme Cochet     - direction artistique
        → Maïssa Boukehil   – administration/production
        → Caroline Mas      - direction technique

|                                                                                     |
| :---------------------------------------------------------------------------------: |
| ![Compagnie Fete Printemps](../../images/illustrations_site/ciefeteprintemps24.jpg) |
