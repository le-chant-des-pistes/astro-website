---
title: ↳ Projet de Territoire 2025-2026
description: ""
displayInNavbar: 6
---

|                                                                         |                                                                         |                                                                         |
| :---------------------------------------------------------------------: | :---------------------------------------------------------------------: | :---------------------------------------------------------------------: |
| ![Illu_FLF_Territoire](../../images/flf_territoire/flf_territoire1.jpg) | ![Illu_FLF_Territoire](../../images/flf_territoire/flf_territoire2.jpg) | ![Illu_FLF_Territoire](../../images/flf_territoire/flf_territoire3.jpg) |

Dans le cadre de la création du spectacle _Feu la Forêt_ (prévue pour mars 2026), la compagnie sera régulièrement présente en 2025 et 2026 dans quatre communes de la métropole de Grenoble (Noyarey, Le Fontanil-Cornillon, Saint-Martin-le-Vinoux et Saint-Egrève) pour des temps de travail artistique partagés entre artistes et publics locaux.

La création s'appuyant sur un travail de documentation sur le terrain et sur des sujets liés aux territoires de moyenne montagne, chaque temps de présence sera l'occasion de déployer l'un des thèmes du spectacle, et de le travailler dans des ateliers de pratique mêlant les interprètes et technicien.ne.s de la compagnie, des habitant.es, des publics invités et des spécialistes des sujets abordés.

C'est ainsi que nous travaillerons à partir d'avril avec ces communes autour de la cartographie et de l'histoire du massif des Hautes-Aigues, de ses biotopes, de la scénographie ou encore l'univers sonore du spectacle.

_+ d'informations à venir prochainement._

|                                                                         |                                                                         |                                                                         |
| :---------------------------------------------------------------------: | :---------------------------------------------------------------------: | :---------------------------------------------------------------------: |
| ![Illu_FLF_Territoire](../../images/flf_territoire/flf_territoire4.jpg) | ![Illu_FLF_Territoire](../../images/flf_territoire/flf_territoire5.jpg) | ![Illu_FLF_Territoire](../../images/flf_territoire/flf_territoire6.jpg) |

<!--
|                 |                                         |                            |                                                                                              |
| --------------- | --------------------------------------- | -------------------------- | -------------------------------------------------------------------------------------------- |
| 19 mars 2025    | **Soirée de Lancement de la résidence** | à Noyarey, Salle Poly-Sons | Représentation tout-publics de _Mort d'une Montagne_ et Présentation du projet de territoire |
| Dates à définir | Semaine de Résidence 1                  | Commune à définir          | Thème à définir                                                                              |
| Dates à définir | Semaine de Résidence 2                  | Commune à définir          | Thème à définir                                                                              |
| Dates à définir | Semaine de Résidence 3                  | Commune à définir          | Thème à définir                                                                              |
| Dates à définir | Semaine de Résidence 4                  | Commune à définir          | Thème à définir                                                                              |
-->
