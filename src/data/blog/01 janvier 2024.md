---
author: Jerome
description: Création du Chant des Pistes
isDraft: false
pubDate: 2023-11-20
title: 01 Hiver 2024 → Naissance de la compagnie
---

_Grenoble, 12 janvier 2024._

2024 a démarré pour nous par une représentation de _Mort d'une Montagne_ au Prisme de Seyssins (38) ce mercredi 11 janvier. Cette première date de l'année a été l'occasion d'annoncer pour la première fois publiquement la constitution de la compagnie _Le Chant des Pistes_, et l'implantation de son siège social à Grenoble.

Nous y poursuivrons le travail de territoire et de création effectué avec _Les Non Alignés_ entre 2015 et 2023, en restructurant la compagnie, et en développant son ancrage en Isère.

L'année 2024 verra les spectacles _Mort d'une Montagne_ et _Horizon(S)_ poursuivre leurs tournées, tandis qu'en parallèle nous ébaucherons les premiers contours d'une nouvelle création : [_Feu la Forêt_](/projets/feu-la-foret), qui constituera le second volet du _Cycle des Hautes Aigues_, après _Mort d'une Montagne_

Cette nouvelle création commencera à s'écrire à la faveur d'un partenariat entre la compagnie et le festival _Textes en l'Air_, qui nous accueillera tout au long de l'année pour plusieurs résidences de terrain. Ces temps d'enquête et d'écriture nous permettront d'aboutir à une première forme courte prévue pour l'été 2024, dans le cadre du festival _Textes en l'Air_.

---

Vous pourrez suivre sur ces pages nos actualités, tournées et récits de création, et vous inscrire à la Newsletter (ci-dessous).

L'année 2023 a également été marquée par la réception du Prix Pyrénéen - Littérature pour l'édition de la pièce _Mort d'une Montagne_ [(Editions Libel)](https://www.editions-libel.fr/maison-edition/boutique/mort-dune-montagne/) !

![PrixPyrénéen](../../images/blog/janvier24/prixpyr.avif)

  <iframe
    width="100%"
    height="500"
    src="https://47ca03b0.sibforms.com/serve/MUIFAHMM67QqJCoDP0a3EP3A6dJV2aY9o6QkNC3zPdKAAzL-frD291cD_FeG8BxSz_DDIfTTua8mmAZcgBpn-MG_gAYLSyY17ThbIa5rbBnrk5zcKe25asQLtqURlsTghrOURchCnm3uYVTqy-PHlVd2VMmtdIsSbgeAa5ANcqHeIG1o_XjSHXxsAX87n-D6hiQZ15x1B3dkdK3B"
  ></iframe>
