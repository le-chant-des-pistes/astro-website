---
author: Jerome
description: Création du Chant des Pistes
isDraft: false
pubDate: 2024-09-24
title: 03 → Newsletter d'automne 2024
---

---

  <iframe
     height="1000"
    src="https://7c4dp.r.ag.d.sendibm3.com/mk/mr/sh/6rqJ8GoudeITPxaanPS2YH6LCSa/O6OCz9U-xh5D"
  ></iframe>

**Retrouvez régulièrement les actualités de la Cie le Chant des Pistes sur ce lien, ou bien via l'inscription à notre newsletter**.

  <iframe
    width="100%"
    height="500"
    src="https://47ca03b0.sibforms.com/serve/MUIFAHMM67QqJCoDP0a3EP3A6dJV2aY9o6QkNC3zPdKAAzL-frD291cD_FeG8BxSz_DDIfTTua8mmAZcgBpn-MG_gAYLSyY17ThbIa5rbBnrk5zcKe25asQLtqURlsTghrOURchCnm3uYVTqy-PHlVd2VMmtdIsSbgeAa5ANcqHeIG1o_XjSHXxsAX87n-D6hiQZ15x1B3dkdK3B"
  ></iframe>
