---
carouselImagesPath: ../images/mdm
---

```
    → TELECHARGER
```

- <a href="https://lechantdespistes.frama.space/s/ALYtMajQ9kkNJQ9" target="_blank">**Dossier Artistique**</a>
- <a href="https://lechantdespistes.frama.space/s/NbAgG8GNGAanJJs" target="_blank">**Fiche Technique**</a>
- <a href="https://lechantdespistes.frama.space/s/fSftBfDTSJ2FPrc" target="_blank">**Revue de Presse**</a>

![Affiche_MDM](../../images/mdm/affichemdm.jpg)

    → DISTRIBUTION

**Une pièce de** Jérôme Cochet et François Hien  
**Mise en Scène** Jérôme Cochet  
**Scénographie** Caroline Frachet  
**Son et Régie Générale** Caroline Mas  
**Lumière** Nolwenn Delcamp-Risse  
**Vidéo** Jérémy Oury  
**Costumes** Mathilde Giraudeau  
**Avec** Camille Roy, Fabienne Courvoisier, Stéphane Rotenberg, Jérôme Cochet ou Martin Sève

    → PRODUCTION

**Production** _Cie Les Non Alignés → Cie Le Chant des Pistes_

**Coproductions** _Centre dramatique national Drôme Ardèche, Dôme Théâtre d’Albertville, Scènes obliques - Festival de l’Arpenteur, TMG - Grenoble, Les Aires Théâtre de Die -Scène conventionnée d’intérêt national Art en territoire_

**Soutiens** _Département de l’Isère, Communauté de communes Grésivaudan, DRAC Auvergne-Rhône-Alpes, Région Auvergne-Rhône-Alpes, Groupe des 20 Auvergne – Rhône – Alpes, ADAMI._

    → QUELQUES LIENS

[**Teaser**](https://vimeo.com/675765811/6e3764e378)  
[**Extrait de captation**](https://vimeo.com/671675382/0078f728a5)  
[**Commander le livre** - Editions Libel](https://www.editions-libel.fr/maison-edition/boutique/mort-dune-montagne/)

    → EN IMAGES EN TERRITOIRE

![Mort d’une montagne - photo 1](../../images/mdm/mdm-9.avif)
![Mort d’une montagne - photo 2](../../images/mdm/mdm-12.avif)
![Mort d’une montagne - photo 3](../../images/mdm/mdm-13.avif)
![Mort d’une montagne - photo 4](../../images/mdm/mdm-2.avif)
