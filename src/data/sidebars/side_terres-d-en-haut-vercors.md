---
carouselImagesPath: ../images/mdm
---

```
    → TELECHARGER
```

![Affiche_TDH](../../images/tdh/tdhvercors.jpg)

    → DISTRIBUTION

**Mise en Scène et Pilotage** Jérôme Cochet  
**Coordination territoire** Dorian Raymond  
**Equipe** Jérôme Cochet, Caroline Mas, Aude Robin, Dorian Raymond, Antoine Villard...  
**Un projet inspiré par** VILLES# du Collectif X, par Arthur Fourcade et Yoan Miot

    → PRODUCTION

**Production** _Cie Le Chant des Pistes_

![TDH Vercors - carte](../../images/tdh/tdh_vercorscarte.jpg)
