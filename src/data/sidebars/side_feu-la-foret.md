---
carouselImagesPath: ../images/mdm
---

```
    → TELECHARGER
```

- <a href="https://lechantdespistes.frama.space/s/29yYBreHkzB3wdT" target="_blank">**Dossier Artistique**</a>
- <a href="https://www.lignesdhorizon.org/pdf/feu-la-foret-24-25.pdf" target="_blank">**Parcours de création à Lagorce (07)**</a>

![Affiche_FLF](../../images/flf/afficheflf.jpg)

    → DISTRIBUTION

**Une pièce de** Jérôme Cochet et François Hien  
**Mise en Scène** Jérôme Cochet  
**Scénographie** Caroline Frachet  
**Son** Caroline Mas  
**Lumière** Nolwenn Delcamp-Risse  
**Vidéo** Jérémy Oury  
**Costumes** Mathilde Giraudeau  
**Avec** Fabienne Courvoisier, Stéphane Rotenberg, Camille Roy, Martin Sève

    → PRODUCTION

**Production** _Cie Le Chant des Pistes_

**Coproductions et soutiens** _Festival Textes en l'Air, TMG - Grenoble, Lignes d'Horizon, L'Hexagone Scène Nationale, le Dôme Théâtre d’Albertville, Les Aires Théâtre de Die - Scène conventionnée d’intérêt national Art en territoire, Espace Paul Jargot - Crolles, Théâtre de Villefranche..._

    → EN IMAGES EN ENQUÊTE

![Feu la forêt - photo 1](../../images/flf/flf-1.avif)
![Feu la forêt - photo 2](../../images/flf/flf-2.avif)
