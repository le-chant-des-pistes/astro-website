---
carouselImagesPath: ../images/mdm
---

```
    → TELECHARGER
```

- <a href="https://lechantdespistes.frama.space/s/aQaow5b8EERdnS7" target="_blank">**Dossier Artistique**</a>
- <a href="https://lechantdespistes.frama.space/s/HLzJRE7JBXq3ex2" target="_blank">**Fiche [Low-]Tech**</a>

![Affiche_HZ](../../images/horizons/affichehorizons.avif)

    → DISTRIBUTION

**Conception et Jeu** Jérôme Cochet et Arthur Fourcade  
**D'après** _Trous Noirs et Distortions du Temps_ de Kip S. Thorne  
**Technique** Caroline Mas  
**Costumes** Sigolène Petey

    → PRODUCTION

**Production** _Cie Les Non Alignés → Cie Le Chant des Pistes_

**Soutiens** _Observatoire de Lyon, Planétarium de Vaux-en-Velin, Festival Astr'Auvergne_

    → QUELQUES LIENS

[**Extrait**](https://www.youtube.com/watch?v=01zLQpwNHQk&feature=youtu.be)

    → EN IMAGES

![Horizon(s) - photo 1](../../images/horizons/horizons-01.avif)
![Horizon(s) - photo 2](../../images/horizons/horizons-1.avif)
![Horizon(s) - photo 3](../../images/horizons/horizons-2.avif)
![Horizon(s) - photo 4](../../images/horizons/horizons-3.avif)
