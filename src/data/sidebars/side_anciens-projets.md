---
carouselImagesPath: ../images/mdm
---

_Avant la création du Chant des Pistes en 2024, nous avons réalisé plusieurs spectacles, résidences et projets de territoire au sein de la compagnie Les Non Alignés._

_Nous gardons ici la mémoire de ceux d'entre eux qui sont les plus représentatifs de notre travail actuel..._

    → DESTIN(S) [CREATION 2019]

**→ 2019 Prix du Public - Prix Théâtre 13**

[_Dossier Artistique_](/dl/dossier-destins.pdf)

**Ecriture** Jérôme Cochet, Arthur Fourcade, Antoine Villard  
**Mise en Scène** Jérôme Cochet  
**Scénographie** Anabel Strehaiano  
**Lumière** Nolwenn Delcamp-Risse  
**Son et Vidéo** Jérémy Oury  
**Costumes** Sofia Bencherif  
**Avec** Kathleen Dol, Arthur Fourcade, Solenn Louër, Alexandre Ouzia et Antoine Villard

    → TERRES D'EN HAUT# [CREATIONS 2018-2022]

**→ 2022 Résidence de Territoire à St Julien en Genevois (74)**  
**→ 2018 Résidence de Territoire en Belledonne (38)**

**Equipe de création** Jérôme Cochet, Ewen Crovella, Marion Douarche, Caroline Frachet, Caroline Mas, Stéphane Rotenberg, Camille Roy, Antoine Villard

    → LE REFUGE DES SOMS [CREATION 2019]

**→ 2019 Résidence de Territoire à Ribiers (05)**

[_Dossier Artistique_](/dl/dossier-rds.pdf)

**Projet et création sonore** Caroline Mas  
**hotographies** Paul-Henri Michel, Vincent Massieye  
**Conception-Construction** Benoît Bret, Jérôme Cochet  
**Couturière Caroline Frachet**  
**Coproduction** Ateliers Médicis (dispositif Création en Cours)

![Terre d’en haut - photo 1](../../images/anciens/terresdenhaut-2.avif)
![Destins - photo 1](../../images/anciens/destins-14.avif)
![Refuge des soms - photo 1](../../images/anciens/refugedessoms-3.avif)
![Terre d’en haut - photo 2](../../images/anciens/terresdenhaut-13.avif)
