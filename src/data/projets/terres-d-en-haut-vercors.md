---
title: "Terres d'en Haut #3"
year: "2025"
description: la réserve naturelle du Vercors
imagePathForCard: /src/images/tdh/tdhvercors.jpg
carouselImagesPath: ../images/mdm
displayInHomePage: 3
displayInNavbar: 3
displaySidebar: true
relatedSidebar: side_terres-d-en-haut-vercors
---

### un portrait théâtral de la Réserve Naturelle des Hauts Plateaux du Vercors

---

**Terres d’En Haut** est une approche des territoires de montagne par les outils du théâtre. Le projet s’installe en résidence dans un massif, et une équipe pluridisciplinaire de scientifiques et de comédien.ne.s/auteur/metteur-en-scène enquête auprès des habitants et acteurs du territoire pour dresser en un portrait vivant et théâtral.

**Terres d’En Haut** est donc un spectacle, mais c’est aussi un temps de résidence ouvert et participatif où chacun peut trouver sa place, réfléchir, débattre sur les questions qui animent le territoire.

**Terres d’En Haut** est un processus de création nomade qui voyage de massif en massif, autour de problématiques diverses et au gré des occasions qui s’inventent.

Tout au long de l'année 2025, la compagnie _Le Chant des Pistes_ sera en résidence entre Diois, Trièves et Vercors pour mener avec les habitants et usagers de son Parc et de sa Réserve Naturelle le projet **Terres d’En-Haut#3 - VERCORS** !

_Notre objectif_ : Ecrire un spectacle de théâtre participatif qui donne à voir et à entendre un portrait collectif de la Réserve Naturelle des Hauts Plateaux du Vercors. Ce portrait qui se dessinera de nos montagnes sera à la fois polémique et fraternel, dialectique et festif, collectif et rassembleur. Alors quel que soit votre âge, votre lieu de résidence, seul.e.s ou à plusieurs, venez assister et participer aux temps de restitution : Venez donner de la voix, Venez donner votre avis, Venez entendre celui des autres, Venez vous étonner de toutes ces différences qui font la richesse du Vercors ! Venez !

|                                            |                                           |                                                 |
| :----------------------------------------: | :---------------------------------------: | :---------------------------------------------: |
| ![Illu_TDH](../../images/tdh/tdh_aire.jpg) | ![Illu_TDH](../../images/tdh/tdh_bd1.jpg) | ![Illu_TDH_](../../images/tdh/tdh_vercors1.jpg) |

---

    Pour suivre le projet, et y participer :

    ↳ Dorian    - dorianraymond.pro@gmail.com
    ↳ Jérôme    - cochet.j@gmail.com
    ↳ Caroline  - technique@lechantdespistes.fr

---
