---
title: Feu la Forêt
year: "2026"
description: le cycle des Hautes Aigues vol.2
imagePathForCard: /src/images/flf/afficheflf.jpg
carouselImagesPath: ../images/mdm
displayInHomePage: 1
displayInNavbar: 1
displaySidebar: true
relatedSidebar: side_feu-la-foret
---

### une pièce de Jérôme Cochet et François Hien - écriture en cours...

---

> Au lendemain d’un grand incendie qui a ravagé la forêt des Ondes au cours d’un été caniculaire, la vie du petit village de Chamrionde est bouleversée. Alors que la sécheresse perdure et pèse sur la vie rurale, chacun tente de s’organiser pour anticiper la saison à venir. Dans la petite caserne de pompiers du village, des vocations naissent, des alliances et des tensions surgissent. Une question, surtout, est sur toutes les lèvres : par quoi, ou par qui, a été causé l'incendie ?

> Après **Mort d’une Montagne**, un cap est franchi. Ce ne sont plus les montagnes qui tombent au loin, mais notre monde qui brûle, implacablement. Nous sommes au début du Pyrocène, l’âge des grands feux...

<center>
    <a href="https://lechantdespistes.frama.space/s/29yYBreHkzB3wdT" target="_blank"> 
        <button class="bg-orange-400 hover:bg-orange-300 text-white justify-center font-mono py-2 px-4 rounded">
            Télécharger le dossier de présentation
        </button>
    </a>
</center>

Tout au long de l'année 2024, la compagnie a été accueillie en résidence d'écriture-création par le festival Textes en l'Air à Saint-Antoine l'Abbaye (38).

En 2025, la compagnie déploiera un parcours d'Education Artistique et Culturelle sur invitation de l'Association Lignes d'Horizons à Lagorce (07), émaillée par plusieurs temps forts : [plus d'informations à retrouver ici](https://www.lignesdhorizon.org/parcours-eac).

---

    Pour nous soutenir et nous aider à faire naître le projet,
    Pour suivre l'avancée de la création - Contactez-nous !

    ↳ Maïssa    - administration@lechantdespistes.fr
    ↳ Jérôme    - cochet.j@gmail.com
    ↳ Caroline  - technique@lechantdespistes.fr

---
