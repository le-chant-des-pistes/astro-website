---
title: HorizonS
year: "2019"
description: un voyage au cœur des trous noirs
imagePathForCard: /src/images/horizons/affichehorizons.avif
carouselImagesPath: ../images/horizons
displayInHomePage: 4
displayInNavbar: 4
displaySidebar: true
relatedSidebar: side_horizons
---

### d'après Kip S. Thorne - création 2019

---

> Bonsoir !  
> Vous êtes à présent les membres d’un équipage scientifique chevronné, en partance pour une mission d’exploration de l’univers, et nous vous souhaitons la bienvenue ce soir sur le tarmac de cet immense astroport.  
> C’est là, sous le scintillement diffus des étoiles et de la voie lactée, que vous rencontrez vos deux référents de mission, le capitaine Akab et Arnold, son intelligence artificielle de bord.  
> Ensemble, nous embarquons à bord d’un vaisseau spatial spécialement conçu pour l’exploration des trous noirs, et nous partons à la rencontre de leurs mystérieuses singularités...

---

### En tournée

Créé en 2019, le spectacle _Horizon(S)_ tourne désormais régulièrement, avec plus de trente dates à son actif. Nous le jouons le plus souvent l'été, lors de grandes tournées à vélo à l'occasion desquelles nous essayons de tracer des itinéraires sportifs et originaux, à la rencontre de lieux et de personnes avec qui nous créons des liens chaleureux. C'est le moment idéal pour jouer à l'extérieur, sous la voûte étoilée.

Hors saison d'été, nous pouvons jouer également à l'intérieur dans tout type de lieu, adapté ou non à l'accueil de spectacle : nous sommes capables d'équiper une salle en quasi-autonomie.

```
Pour programmer le spectacle - Contactez-nous !

↳ Maïssa - administration@lechantdespistes.fr
↳ Arthur - fourcadearthur@hotmail.com
↳ Jérôme - cochet.j@gmail.com
```

---

![MDM au Plateau](../../images/horizons/ftech_horizons.avif)

---
