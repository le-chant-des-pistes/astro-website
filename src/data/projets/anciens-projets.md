---
title: ↳ Anciens Projets
year: 2016-2022
description: Avec la cie Les Non Alignés
imagePathForCard: /src/images/anciens/afficherds.avif
carouselImagesPath: ../images/mdm
displayInNavbar: 5
# displayInHomePage: 5
isDraft: true
relatedSidebar: side_anciens-projets
---

|                      [DESTIN(S) [2019]](http://lesnonalignes.com/projets/destins/)                       |                 [TERRES D'EN-HAUT [2018-2022]](http://lesnonalignes.com/projets/terresdenhaut1/)                  |                   [LE REFUGE DES SOMS [2019]](http://lesnonalignes.com/projets/refugedessoms/)                   |
| :------------------------------------------------------------------------------------------------------: | :---------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------: |
| [![AfficheDestins](../../images/anciens/affichedestins.avif)](http://lesnonalignes.com/projets/destins/) | [![AfficheTerresdEnHaut](../../images/anciens/affichetdh.avif)](http://lesnonalignes.com/projets/terresdenhaut1/) | [![AfficheRefugedesSoms](../../images/anciens/afficherds.avif)](http://lesnonalignes.com/projets/refugedessoms/) |
