---
title: Mort d'une Montagne
year: "2022"
description: le cycle des Hautes Aigues vol.1
imagePathForCard: /src/images/mdm/affichemdm.jpg
carouselImagesPath: images/mdm
displayInHomePage: 2
displayInNavbar: 2
displaySidebar: true
relatedSidebar: side_mort-d-une-montagne
---

### une pièce de Jérôme Cochet et François Hien - création 2022

---

> Nous sommes dans un massif imaginaire, le massif des Hautes Aigues. Le petit
> village de Rochebrune est en alerte depuis que se produisent des éboulements
> réguliers sur les sommets voisins... Alors que les acteurs du territoire
> s'interrogent sur l'avenir de la vallée, les pouvoirs publics annoncent la
> nomination d'un chargé de mission à la transition climatique en montagne, venu
> faire un diagnostic de territoire. Parallèlement arrive une voyageuse, décidée à
> réaliser l'ascension du sommet majeur de la région, la Grande Reine, dont une
> partie s'est récemment effondrée. Nous suivons en parallèle plusieurs
> personnages pris dans la montagne et confrontés à l'urgence des enjeux qui la
> concernent. Tous se retrouveront au refuge du Vautour, sous le sommet de la
> Grande Reine. Fanny, la gardienne, y scrute avec angoisse les falaises qui
> s'effondrent autour d’elle..

---

### En tournée

De 2020 à 2021, nous avons conclu chacune de nos périodes de résidence par la représentation d’un épisode écrit au cours de la semaine, d’une durée de 20 à 50 minutes environ. Ces représentations ont eu lieu dans des lieux variés du massif et jusqu’en vallée, et ont permis d’aller à la rencontre des habitants, des acteurs du territoire, afin de valider le texte, de l’enrichir, de l’ajuster… Elles sont aujourd’hui complétées par la mise en scène d’un spectacle complet, créé en janvier 2022 au Théâtre du Point du Jour, puis au Théâtre Municipal de Grenoble…

Le spectacle peut à présent tourner sous trois formes : tout terrain (sans technique), nomade (avec technique son-décor-lumière adaptée) et plateau (en salle de théâtre équipée). Les trois formes proposent chacune de vivre l’intégralité du récit (durée 2h) et nous les jouons chacune régulièrement.

```markdown
Pour programmer le spectacle - Contactez-nous !

↳ Maïssa - administration@lechantdespistes.fr
↳ Jérôme - cochet.j@gmail.com
```

---

![MDM au Plateau](../../images/mdm/mdm-29.avif)

### Presse

> La montagne a mis des étoiles dans l’écriture des deux auteurs. Souhaitons à ce magnifique spectacle qui parle à tous la longévité qu’il mérite.
> **Les Trois Coups**

> Habilement mis en scène, interprété avec justesse, _Mort d'une Montagne_ est une chronique documentée portée par un suspense haletant.
> **Le Progrès**

> _Mort d’une Montagne_ est une chronique saisissante de l’anthropocène. Loin d’être moralisateur, le spectacle captive [...]. La fluidité de l’écriture, la magie de la scénographie et la formidable palette de jeu des comédien·ne·s en font un important moment de théâtre.
> **La Pépinière - Genève**

> Inédite car rares sont les pièces à traiter de la vie en altitude. La troupe a d’ailleurs rencontré tous les acteurs de la montagne et a veillé à ce que la pièce soit la plus documentée possible. C’est saisissant, grave, avec de la poésie et du rire aussi.
> **France Bleu**

> L’imagination du spectateur suit son cours avec le récit de l’ascension : une main en l’air et on voit apparaître la paroi à pic, un regard vers le bas et on aperçoit le millier de mètres de dénivelé.
> **L’Alchimie du Verbe**

<!--
|                                                      |                                                      |                                                      |
| :--------------------------------------------------: | :--------------------------------------------------: | :--------------------------------------------------: |
| [![A](/images/mdm/mdm-1.avif)](/images/mdm/mdm-1.avif) | [![A](/images/mdm/mdm-2.avif)](/images/mdm/mdm-2.avif) | [![A](/images/mdm/mdm-3.avif)](/images/mdm/mdm-3.avif) |
| [![A](/images/mdm/mdm-4.avif)](/images/mdm/mdm-4.avif) | [![A](/images/mdm/mdm-5.avif)](/images/mdm/mdm-5.avif) | [![A](/images/mdm/mdm-6.avif)](/images/mdm/mdm-6.avif) |
| [![A](/images/mdm/mdm-7.avif)](/images/mdm/mdm-7.avif) | [![A](/images/mdm/mdm-8.avif)](/images/mdm/mdm-8.avif) | [![A](/images/mdm/mdm-9.avif)](/images/mdm/mdm-9.avif) |
->
