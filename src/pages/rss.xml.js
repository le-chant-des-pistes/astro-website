import rss from "@astrojs/rss";
import { getCollection } from "astro:content";

export async function GET(context) {
  const blog = await getCollection("blog");
  return rss({
    title: "Le chant des pistes | Actualités",
    description: "Fil d’actualités de la compagnie Le chant des pistes",
    site: context.site,
    items: blog.map((post) => ({
      title: post.data.title,
      pubDate: post.data.pubDate,
      description: post.data.description,
      customData: post.data.customData,
      link: `/blog/${post.id}/`,
    })),
    customData: `<language>fr-fr</language>`,
    stylesheet: "/rss/styles.xsl",
  });
}
