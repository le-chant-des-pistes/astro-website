// 1. Import utilities from `astro:content`
import { rssSchema } from "@astrojs/rss";
import { glob } from "astro/loaders";
import { defineCollection, reference, z } from "astro:content";

// 2. Define your collection(s)
const pages = defineCollection({
  loader: glob({ pattern: "**/*.md", base: "./src/data/pages" }),
  schema: z.object({
    description: z.string(),
    displayInNavbar: z.number().int().positive().optional(),
    title: z.string(),
  }),
});

const blog = defineCollection({
  loader: glob({ pattern: "**/*.md", base: "./src/data/blog" }),
  schema: rssSchema.extend({
    isDraft: z.boolean().optional(),
  }),
});

const projets = defineCollection({
  loader: glob({ pattern: "**/*.md", base: "./src/data/projets" }),
  schema: z.object({
    carouselImagesPath: z.string(),
    description: z.string(),
    displayInHomePage: z.number().int().positive().optional(),
    displayInNavbar: z.number().int().positive().optional(),
    imagePathForCard: z.string(),
    isDraft: z.boolean().optional(),
    relatedSidebar: reference("sidebars"),
    subTitle: z.string().optional(),
    title: z.string(),
    year: z.string(),
  }),
});

const sidebars = defineCollection({
  loader: glob({ pattern: "**/*.md", base: "./src/data/sidebars" }),
  schema: z.object({}),
});

// 3. Export a single `collections` object to register your collection(s)
//    This key should match your collection directory name in "src/content"
export const collections = { pages, blog, projets, sidebars };
