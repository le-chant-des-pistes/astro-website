import { defineConfig } from "astro/config";
import tailwind from "@astrojs/tailwind";

// https://astro.build/config

export default defineConfig({
  integrations: [tailwind({ applyBaseStyles: false })],
  outDir: "public",
  publicDir: "static",
  site: "https://lechantdespistes.fr",
  sitemap: true,
});
