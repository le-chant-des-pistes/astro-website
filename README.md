# Ce dépôt contient le code du site internet de la Cie "Le chant des pistes"

## Gestion du site : la documentation locale

Une documentation web (<http://localhost:4444>) est disponible à l’aide des commandes suivantes.

Dans le terminal (ctrl+j) :

| Command               | Action                                                                                                              |
| :-------------------- | :------------------------------------------------------------------------------------------------------------------ |
| `npm run doc:install` | La 1ère fois, pour installer les dépendances                                                                        |
| `npm run doc:start`   | Démarre rapidement                                                                                                  |
| `npm run doc:preview` | Démarrage plus long (ça builde d’abord), mais permet de disposer d’une barre de recherche pour chercher dans la doc |

---

## Astro documentation

### 🚀 Project Structure

Inside of your Astro project, you'll see the following folders and files:

```text
/
├── public/
│   └── favicon.svg
├── src/
│   ├── components/
│   │   └── Card.astro
│   ├── content/
│   │   └── pages/
│   │       └── page.md
│   ├── images/
│   │   └── image.jpg
│   ├── layouts/
│   │   └── Layout.astro
│   └── pages/
│       └── index.astro
└── package.json
```

Astro looks for `.astro` or `.md` files in the `src/pages/` directory. Each page is exposed as a route based on its file name.

There's nothing special about `src/components/`, but that's where we like to put any Astro/React/Vue/Svelte/Preact components.

Any static assets, like images, can be placed in the `public/` directory.

### 🧞 Commands

All commands are run from the root of the project, from a terminal:

| Command                   | Action                                           |
| :------------------------ | :----------------------------------------------- |
| `npm install`             | Installs dependencies                            |
| `npm run dev`             | Starts local dev server at `localhost:4321`      |
| `npm run build`           | Build your production site to `./dist/`          |
| `npm run preview`         | Preview your build locally, before deploying     |
| `npm run astro ...`       | Run CLI commands like `astro add`, `astro check` |
| `npm run astro -- --help` | Get help using the Astro CLI                     |
| `npm run format`          | Format all files with prettier                   |

### 👀 Want to learn more?

Feel free to check [our documentation](https://docs.astro.build) or jump into our [Discord server](https://astro.build/chat).
