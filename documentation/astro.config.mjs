import starlight from "@astrojs/starlight";
import { defineConfig } from "astro/config";

// https://astro.build/config
export default defineConfig({
  integrations: [
    starlight({
      title: "Documentation pour la gestion du site de la compagnie",
      defaultLocale: "root",
      social: { gitlab: "https://framagit.org/le-chant-des-pistes" },
      sidebar: [
        {
          label: "Démarrer",
          autogenerate: { directory: "start" },
        },
        {
          label: "Guides",
          autogenerate: { directory: "guides" },
        },
        {
          label: "Fiches mémos",
          autogenerate: { directory: "reminders" },
        },
        {
          label: "Références",
          autogenerate: { directory: "reference" },
        },
        {
          label: "Notes perso",
          autogenerate: { directory: "notes-perso" },
        },
      ],
    }),
  ],
});
