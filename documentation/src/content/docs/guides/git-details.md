---
title: Git en détail
sidebar:
  order: 4
---

## Git : comment ça marche ?

Git est le gestionnaire de version du code. Il permet de gérer plusieurs versions de l’état du code.

On peut manipuler git :

- soit en ligne de commande dans un terminal (`ctrl+J` dans vscode pour ouvrir un terminal)
- soit à l’aide d’une interface visuelle (extension gitlens)

### Récupérer le code à partir du serveur distant : git pull

```bash
# dans le terminal
# aller sur main
git checkout main
# mettre à jour
git pull
```

### Sauvegarder le code : créer un commit et le pousser vers le serveur distant

On va faire 3 actions :

1. ajouter toutes les modifications
2. créer un commit qui contient ces modifications
3. pousser cet état vers le serveur distant (framagit)

#### Avec l’interface gitlens

Dans la colonne de gauche, accéder à "Source control" :

1. cliquer sur le `+` "stage all changes
2. Au-dessus de "Commit", renseigner un message de commit, puis cliquer sur "Commit"
3. Cliquer sur "Publish branch"

#### En ligne de commande

```bash
# ouvrir le terminal : ctrl+J
# 1. ajouter toutes les modifications
git add .

# 2. créer un nouveau commit avec un message de commit
git commit -m "une mini description du commit. Exemple : ajout d’un nouveau billet de blog"

# 3. pousser cet état vers le serveur distant (framagit)
git push main
```
