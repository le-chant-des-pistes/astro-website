---
title: Modifier le design ou la structure du site
description: ""
sidebar:
  order: 3
---

Comme ça peut vite prendre du temps, on applique le process suivant.

1. discuter avec Séverin des fonctionnalités que l’on voudrait
2. en fonction du temps estimé, on décide si on le fait ou pas
3. si c’est rapide, Séverin réalise les modifications
4. si c’est plus long, on voit comment on veut s’y prendre, si on le fait ou pas, etc.

N’hésitez pas à me contacter sur signal ou en visio.
