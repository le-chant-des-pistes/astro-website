---
title: Créer un nouveau contenu
sidebar:
  order: 2
---

## 1. préparer son git flow

Avant toute modification :

```bash
# se mettre sur la branche main
git checkout main
# mettre à jour main
git pull main
# créer une nouvelle branche, il faut qu’elle ait un nom
git checkout -b feat/nom_de_branche_bla_bla_pas_crucial
```

## 2. ajouter du contenu

### Nouveau contenu texte

Pour le texte, on utilise des fichiers en markdown (.md).

Le plus simple est de dupliquer le fichier d’un contenu déjà existant, puis modifier ce nouveau fichier.

Vous trouverez aussi des fichiers d’exemple `sample.md`, qui servent d’exemples.

### Ajouter une image

S’il y a des images, les ajouter dans un sous-dossier de `/src/images`.

:::caution
Les images doivent être :

- déjà au bon gabarit (cadrage, couleurs, dimensions, etc.)
- dans un format léger adapté au web, comme le **.avif**

:::

## 3. sauvegarder le contenu sur framagit

Quand on a fini, on crée un "commit" :

```bash
# ajouter les fichiers nouvellement modifiés
git add .
# créer un nouveau commit
git commit -m "feat(content): blabla descriptif pas crucial"
# sauvegarder sur framagit
git push origin <nom_de_ma_branche_crée_auparavant>
```

## 4. créer une "merge request" sur framagit

Aller sur la page [branches](https://framagit.org/le-chant-des-pistes/astro-website/-/branches) :

1. vous devez voir le nom de votre nouvelle branche dans la liste
2. à droite, cliquer sur "New" ("New merge request") pour créer la merge request

:::note
Le code est bien sauvegardé sur framagit, mais il n’est pas "en ligne".
:::

## 5. Relecture et validation par un pair

Ensuite, suivant le type de process mis en place, on peut attendre qu’un relecteur relise la MR avant de la merger.
