---
title: Mettre à jour les dépendances
sidebar:
  order: 5
---

Réaliser les étapes suivantes :

1. afficher les dépendances qui peuvent être mises à jour : dans le terminal, `npm outdated`
2. dans le package.json, repérer les dépendances concernées, et à la main, modifier la version
3. exécuter `npm install`
4. on doit avoir 2 fichiers modifiés :
   - package.json (par soi manuellement)
   - package-lock.json (automatiquement par le `npm install`)
5. faire un commit avec ces modifs

La même chose peut être faite côté `/documentation` (dans le terminal, se positionner au bon endroit `cd /documentation`).
