---
title: Démarrer le projet
sidebar:
  order: 5
---

Toujours dans vscode, dans le terminal :

```bash
npm run dev
```

Et ouvrir le lien dans le navigateur : <http://localhost:4321>

:::tip[C’est du live!]
Maintenant, dès qu’on modifie un fichier, le changement est pris en compte.
Dans le navigateur, la page se recharge, et le changement est présent.
:::
