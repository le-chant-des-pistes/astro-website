---
title: Accéder au code sur framagit
sidebar:
  order: 2
---

## Configurer ses accès au serveur de sauvegarde du code : framagit

1. Créer un compte sur <https://framagit.org>
2. Générer une clé SSH sur son PC : [cf. doc generate-an-ssh-key-pair](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair). Ne pas créer de passphrase pour la clé SSH.
3. dans votre compte framagit, copier/coller la partie publique de la clé SSH ici : <https://framagit.org/-/profile/keys>
