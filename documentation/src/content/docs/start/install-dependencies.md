---
title: Installer les dépendances du projet
sidebar:
  order: 4
---

Dans vscode, ouvrir le répertoire contenant le code.

```bash
# Dans le terminal, installer les dépendances
npm install
```
