---
title: Configurer son environnement de travail
sidebar:
  order: 1
---

## Logiciels à installer

- le serveur [NodeJs](https://nodejs.org)
- un terminal amélioré (qui affiche les branches git courantes) :
  - [Starship](https://starship.rs/fr-FR/guide/#%F0%9F%9A%80-installation) --> fonctionne avec le PowerShell de windows, nickel !
  - ou [GitBash](https://git-scm.com/downloads) (disponible sur windows aussi)
- l’éditeur de code [VScode](https://code.visualstudio.com/download)

:::note
[En savoir plus sur le terminal](reference/terminal.md)
:::

## Configurer l’éditeur de code

### Extensions

Ouvrir vscode, dans la sidebar, il y a une icône "Extensions", rechercher et installer les extensions suivante :

- astro
- prettier
- gitlens

### Configuration du terminal

Dans vscode :

1. ouvrir le terminal `ctrl+J`
2. à droite du `+`, ouvrir le menu et cliquer sur "sélectionner le terminal par défaut"
3. choisir le terminal par défaut (soit PowerShell+Starship, soit gitbash ou autre). **Il faut avoir un terminal qui affiche la branche git courante de façon visible !**
