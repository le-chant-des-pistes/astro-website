---
title: 1ère récupération du code
sidebar:
  order: 3
---

## Git clone

1. ouvrir un terminal (powershell ou gitbash)
2. se placer dans le dossier où vous voulez mettre les fichiers : `cd /mon/super/dossier/de/travail`
3. cloner le dépôt :

```bash
# Dans le terminal
git clone https://framagit.org/le-chant-des-pistes/astro-website/
```

:::note
En savoir plus : <https://docs.github.com/fr/repositories/creating-and-managing-repositories/cloning-a-repository>
:::
