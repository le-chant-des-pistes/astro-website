﻿---
title: Notes de Steph
---

OUTILS:
Framagit : versionnage du code
Astro-website: framework web
VSCode (visual studio) : éditeur de code

MON WORKFLOW STANDART DE MODIFICATIONS
Dans VsCode:
Dans le terminal :

- npm install pour installer les dépendances, librairies…
- git pull pour télécharger en local les fichiers du site qui sont en production
- npm run dev pour build le site en local (et ctrl+c pour arrêter et retrouver le terminal)

à ce stade, je fais tourner sur mon ordi (localhost) le site tel qu’il est en prod
Je peux faire des modifs sur les fichiers que je veux éditer (principalement dans content/pages et content/project)
puis

- git add . prend en compte les modifs sur tous les fichiers modifiés (d’où le “.”)
- git commit -m “J’ai modifié ceci ou cela” enregistre en local les modifications et y ajoute une note (d’où le -m pour ‘message’)
- git push main pousse les modifications sur le serveur distant

Les erreurs seront typiquement des histoires de droit sur git (exemple : si le user a des simples droits de developpeur, il ne pourra pas push le main entier), ou des erreurs de versionnage (plusieurs personnes ont modifié les fichiers en parallèle, et il faut faire des choix entre les différentes branches git).

—------
PETIT PREREQUIS
installer node.js
(les fichiers how_to_add_content et readme.md contiennent des infos utiles, blablabla)

—-----
SUIVI EN LOCAL DES MODIFS
Le noeud “source control” expose les fichiers qui ont été modifiés et pas encore commit, avec la possibilité de “discard” les modifications.

—------
SUIVI DES MODIFS SUR FRAMAGIT
Sur ton espace framagit, on a vu le pipeline (dans build/pipeline) qui te permet de suivre l’historique des déploiements, par user, avec leur release notes.
