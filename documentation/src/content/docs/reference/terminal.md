---
title: Terminal
---

Il existe pleins d’alternatives pour avoir un terminal moderne. Mais tous ne sont pas disponibles sous windows.

Exemples :

- [Fish](https://fishshell.com/) (celui que j’utilise au quotidien)
- [NuShell](https://www.nushell.sh/)
- [GitBash](https://git-scm.com/downloads) (disponible sur windows)
