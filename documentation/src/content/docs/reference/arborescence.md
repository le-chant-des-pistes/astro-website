---
title: Arborescence des fichiers
---

Les fichiers sont rangés de la façon suivante :

### les contenus en markdown

| Dossier             |                                             |
| ------------------- | ------------------------------------------- |
| `/content/blog`     | contient les articles de blog               |
| `/content/projets`  | contient les pages des projets              |
| `/content/sidebars` | contient les sidebars associées aux projets |

### les images

Les images sont dans le dossier `/src/images`.
`/src/images/horizons` contient les images du projet horizons.
Etc.
