---
title: Gestionnaire de mots de passe
---

Aujourd’hui, on est obligé d’avoir une multitude de mots de passe pour une multitude de sites.

Comment faire ?

1. Installer un gestionnaire de mots de passe. Par exemple :
   - <https://keepassxc.org/>
   - <https://proton.me/fr/pass>
2. Utiliser le gestionnaire de mots de passe pour générer et stocker les mots de passe.
