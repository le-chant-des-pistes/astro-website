---
title: Git
---

```bash
# aller sur la branche main
git checkout main

# mettre à jour main
git pull main

# créer une nouvelle branche
git checkout -b feat/nom_de_branche_bla_bla_pas_crucial
```

```bash
## Créer un commit
# ajouter les fichiers nouvellement modifiés
git add .

# créer un nouveau commit
git commit -m "feat(content): blabla descriptif pas crucial"

# sauvegarder sur framagit
git push origin <nom_de_ma_branche_crée_auparavant>
```
